import std.stdio;
import std.array:replace;

auto VarToBS(T)(const T var){ 
	static immutable int SIZE=var.sizeof*8;
	static assert(SIZE<=64,"too big");
	char [SIZE] str;
	char* Sptr=str.ptr;
	enum REGS=[
		8:"BL",
		16:"BX",
		32:"EBX",
		64:"RBX"
	];
	static immutable code=q{
	asm {  
		mov RAX,[Sptr];
		mov ECX,[SIZE];
		mov REGISTER,[var];
			CYCL:
		sal REGISTER,1; 
		mov [RAX],49; //49='1'
		jc IFL;
		mov [RAX],48; //48='0'
			IFL:
		inc RAX;
		loop CYCL;
		}
	}.replace("REGISTER",REGS[SIZE]);
	mixin(code);
	return str;
}

void main()
{
	writeln(VarToBS(3));

	writeln(VarToBS!int(5));
	writeln(VarToBS!long(5));
	writeln(VarToBS!bool(1));

	struct st{
			long  a,b;
	}
	st c={1,2};
	writeln(VarToBS(c));
}
